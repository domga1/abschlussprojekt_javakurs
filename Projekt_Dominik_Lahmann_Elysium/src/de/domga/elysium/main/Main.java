package de.domga.elysium.main;

import javafx.application.Application;

/**
 * Startet das Programm Elysium
 * - Elysium ist ein Spiel welches sich aus verschiedenen Genres zusammensetzt.
 * - Es werden Elemente aus Klassischen Textadventuren, den Pokemon spielen, sowie Dungeoncrawlern verwendet
 * - Im laufe des spieles kann der Spieler gegen verschiedene Gegner k�mpfen sowie R�ume mit speziellen Events betreten.
 * - Das Ziel des Spiels ist es, den Spieler in den Raum mit der Krone (Endraum) zu steuern.
 * 
 * 
 * Was noch geplant und nicht implementiert ist:
 * 
 * 		- Die Story, bisher sind haupts�chlich Platzhalter implementiert
 * 		- Erh�hung der Statuswerte durch K�mpfen und Events
 * 		- Die R�ume mit T�ren versehen (Norden, S�den, Osten, Westen) und zugeh�rige Schl�sselkarten einbauen
 * 		- vieles mehr.....
 * 
 * Version 1.1 Alpha
 *  
 * @author Dome
 *
 */
public class Main {

	public static void main(String[] args) {
		
		Application.launch(de.domga.elysium.frontend.UI.class);

	}

}
