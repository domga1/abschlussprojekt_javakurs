package de.domga.elysium.frontend;

import de.domga.elysium.backend.character.Player;
import de.domga.elysium.backend.character.enemy.Enemy;
import de.domga.elysium.backend.events.Event;
import de.domga.elysium.backend.events.EventDAO;
import de.domga.elysium.backend.events.EventDAOImplMitDatei;
import de.domga.elysium.backend.rooms.RoomDAO;
import de.domga.elysium.backend.rooms.RoomDAOImplMitMaker;
import de.domga.elysium.middletier.EnemyService;
import de.domga.elysium.middletier.EventService;
import de.domga.elysium.middletier.GameService;
import de.domga.elysium.middletier.RoomService;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.control.SplitPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * UI des Programms Hier werden alle Teile der Grafischen Darstellungen verwaltet und
 * zusammengebaut
 * 
 * Zus�tzlich werden alle ben�tigten Serviceobjekte erstellt
 * 
 * @author Dome
 *
 */
public class UI extends Application {

	//Serviceobjekte und Datenquellen
	private RoomDAO roomSource;
	private EventDAO eventSource;
	private RoomService roomService;
	private GameService gameService;
	private EventService eventService;
	private EnemyService enemyService;

	// Boxes
	private ExitBox newExitBox;

	// Panes
	private GridPane mapPane;
	private HBox imagePane;
	private VBox movePane;
	private GridPane attackPane;

	// Buttons
	private Button attackMelee;
	private Button attackRange;
	private Button block;
	private Button useItem;
	private Button flee;
	private Button ok;
	private Button yes;
	private Button no;
	private Button existToUseKeyboard;

	// Labels
	private Label statusText;
	private Label enemyName;
	private Label health;
	private Label healthValue;
	private Label armor;
	private Label armorValue;
	private Label evade;
	private Label evadeValue;
	private Label strength;
	private Label strengthValue;
	private Label agility;
	private Label agilityValue;
	private Label knowledge;
	private Label knowledgeValue;
	private Label moveLabel;

	// Scene und Stages
	private Stage window;
	private Scene startScene;
	private Scene mainScene;

	private String textInput; // Textinhalt f�r das Statusfenster / Textausgabe
	private Boolean combatEnd; // Boolean to trigger the end of combat
	private Boolean canMove; // Boolean to see im moving on the GRID is allowed
	private int combatRound; // To count the number of combat rounds

	public void init() {
//		eventSource = new EventDAOImplMitMaker();
		eventSource = new EventDAOImplMitDatei();
		roomSource = new RoomDAOImplMitMaker();
		roomService = new RoomService(roomSource);
		gameService = new GameService(roomService);
		eventService = new EventService(eventSource);
		enemyService = new EnemyService();
		newExitBox = new ExitBox();

		textInput = "";
		combatEnd = false;
		canMove = true;
		combatRound = 0;
	}

	@Override
	public void start(Stage primaryStage) {

		window = primaryStage;
		window.setTitle("Elysium Alpha");
		window.setResizable(false); // Verhindert das ver�ndern der Fenstergr��e

		// Startet beim klick auf das "X" die closeProgram Methode
		window.setOnCloseRequest(e -> {
			e.consume(); // �bernahme des Close Befehls
			closeProgram();
		});

		// **********************************StartFenster*************************************

		VBox welcomePane = new VBox(3);
		welcomePane.setId("welcomePane");
		welcomePane.setAlignment(Pos.CENTER);
		welcomePane.setSpacing(80);
		Label welcomeLabelOne = new Label("Welcome to Elysium");
		welcomeLabelOne.setId("welcomeLabelOne");
		Label welcomeLabelTwo = new Label("You wake up in a room filled with empty cryogenic capsules\n\n "
				+ "your memories are gone...\n\n\n\n"
				+ "start your way through elysium");
		welcomeLabelTwo.setId("welcomeLabelTwo");
		Button welcomeButton = new Button("Begin adventure");
		welcomeButton.setId("welcomeButton");
		welcomeButton.setOnAction(e -> window.setScene(mainScene));
		welcomePane.getChildren().addAll(welcomeLabelOne, welcomeLabelTwo, welcomeButton);
		startScene = new Scene(welcomePane, 900, 700);

		// ***********************************Hauptfenster************************************
		VBox mainBox = new VBox(2);
		mainBox.setId("mainBox");

		// Menubar
		MenuBar menuBar = new MenuBar();
		Menu menu = new Menu("File");
		menu.getItems().add(new MenuItem("New Game..."));
		menu.getItems().add(new SeparatorMenuItem());
		menu.getItems().add(new MenuItem("Save Game..."));
		menu.getItems().add(new SeparatorMenuItem());
		menu.getItems().add(new MenuItem("Exit..."));
		menuBar.getMenus().addAll(menu);

		// Inhalt Hauptfenster
		SplitPane mainSplitPane = new SplitPane();
		mainSplitPane.setDividerPosition(0, 0.66);
		SplitPane leftSplitPane = new SplitPane();
		leftSplitPane.setId("leftSplitPane");
		leftSplitPane.setOrientation(Orientation.VERTICAL);
		leftSplitPane.setDividerPosition(0, 0.57);
		SplitPane rightSplitPane = new SplitPane();
		rightSplitPane.setId("rightSplitPane");
		rightSplitPane.setOrientation(Orientation.VERTICAL);
		rightSplitPane.setDividerPosition(0, 0.57);

		// ************************************************Inhalt Links**********************************************
		SplitPane upLeftSplitPane = new SplitPane();
		SplitPane downLeftSplitPane = new SplitPane();
		leftSplitPane.getItems().addAll(upLeftSplitPane, downLeftSplitPane);
		downLeftSplitPane.setId("downLeftSplitPane");

		// *************************Inhalt Oben Links********************
		SplitPane textBoxPane = new SplitPane();
		textBoxPane.setDividerPosition(0, 0.33);
		textBoxPane.setOrientation(Orientation.VERTICAL);
		textBoxPane.setId("textBoxPane");
		
		// Event Buttons
		HBox eventButtonBox = new HBox(3);
		yes = new Button("Yes");
		yes.setMinWidth(100);
		yes.setMinHeight(30);
		no = new Button("No");
		no.setMinWidth(100);
		no.setMinHeight(30);
		ok = new Button("OK");
		ok.setMinWidth(100);
		ok.setMinHeight(30);
		ok.setVisible(true);
		eventButtonBox.getChildren().addAll(yes, ok, no);
		eventButtonBox.setAlignment(Pos.CENTER);
		eventButtonBox.setPadding(new Insets(10, 10, 10, 10));
		eventButtonBox.setSpacing(20);
		ok.setVisible(false);
		yes.setVisible(false);
		no.setVisible(false);

		// Textarea
		statusText = new Label("");
		statusText.setWrapText(true);
		statusText.setId("statusText");
		
		HBox textBoxWrap = new HBox(1);
		textBoxWrap.getChildren().addAll(statusText);
		textBoxWrap.setAlignment(Pos.CENTER);
		textBoxWrap.setId("textBox");

		textBoxPane.getItems().addAll(textBoxWrap, eventButtonBox);

		// Image Window
		SplitPane imageSplitPane = new SplitPane();
		imageSplitPane.setId("imageSplitPane");
		imageSplitPane.setOrientation(Orientation.VERTICAL);
		imageSplitPane.setDividerPosition(0, 0.8);
		
		// NameTag Label
		enemyName = new Label("Eylsium...");
		enemyName.setId("enemyName");

		imagePane = new HBox();
		imagePane.setAlignment(Pos.CENTER);
		imagePane.setId("imagePane");
		imagePane.getChildren().add(new ImageView(new Image("File:ressourcen/Images/Player/player.png")));

		imageSplitPane.getItems().addAll(enemyName, imagePane);
		upLeftSplitPane.getItems().addAll(textBoxPane, imageSplitPane);
		upLeftSplitPane.setDividerPosition(0, 0.75);
		upLeftSplitPane.setId("upLeftSplitPane");

		// **********************Inhalt Unten Links Combat************************
		attackPane = new GridPane();
		attackPane.setHgap(5);
		attackPane.setVgap(10);
		attackPane.setAlignment(Pos.CENTER);
		attackPane.setPadding(new Insets(0, 1, 20, 1));
		attackPane.setMinWidth(300);
		attackPane.setMinHeight(300);
		attackPane.setGridLinesVisible(false);
		attackPane.setVisible(false);
		attackPane.setId("attackPane");

		// Attack Label
		Label attackLabel = new Label("Attack Buttons");
		attackLabel.setPadding(new Insets(10, 1, 10, 1));
		attackLabel.setId("attackLabel");

		// AttackButtons
		attackMelee = new Button("Attack Melee");
		attackMelee.setMinWidth(150);
		attackMelee.setMinHeight(30);
		attackRange = new Button("Attack Range");
		attackRange.setMinWidth(150);
		attackRange.setMinHeight(30);
		block = new Button("Block");
		block.setMinWidth(150);
		block.setMinHeight(30);
		useItem = new Button("Use Item");
		useItem.setMinWidth(150);
		useItem.setMinHeight(30);
		flee = new Button("Flee");
		flee.setMinWidth(150);
		flee.setMinHeight(30);

		// Add Attack Buttons
		attackPane.add(attackLabel, 0, 0);
		attackPane.add(attackMelee, 0, 1);
		attackPane.add(attackRange, 0, 2);
		attackPane.add(block, 0, 3);
		attackPane.add(useItem, 0, 4);
		attackPane.add(flee, 0, 5);

		// **********************Inhalt Unten Links Move************************
		movePane = new VBox();
		movePane.setPadding(new Insets(1, 10, 50, 5));
		movePane.setMinWidth(300);
		movePane.setMinHeight(300);
		movePane.setId("movePane");

		//MoveLabel
		moveLabel = new Label("Move the Player with\n\n\"W\"   \"A\"   \"S\"   \"D\"");
		moveLabel.setId("moveLabel");
		Label moveLabelEmpty = new Label("");
		
		// MoveButtons
		existToUseKeyboard = new Button("Useless Button");
		existToUseKeyboard.setId("uselessButton");
		
		movePane.getChildren().addAll(moveLabelEmpty, moveLabel, existToUseKeyboard);
		
		downLeftSplitPane.getItems().addAll(attackPane, movePane);
		downLeftSplitPane.setDividerPosition(0, 0.5);

		// *************************************************Inhalt Rechts**************************************

		// **********************Inhalt Oben Rechts / Map************************
		mapPane = new GridPane();
		mapPane.setHgap(1);
		mapPane.setVgap(1);
		mapPane.setAlignment(Pos.CENTER);
		mapPane.setPadding(new Insets(1, 15, 1, 1));
		mapPane.setMinWidth(300);
		mapPane.setMinHeight(400);
		mapPane.setGridLinesVisible(true);
		mapPane.setId("mapPane");
		reloadMap();

		// **********************Inhalt Unten Rechts / Playerstatus************************
		GridPane statusPane = new GridPane();
		statusPane.setHgap(5);
		statusPane.setVgap(10);
		statusPane.setAlignment(Pos.CENTER_LEFT);
		statusPane.setMinWidth(300);
		statusPane.setMinHeight(300);
		statusPane.setGridLinesVisible(false);
		statusPane.setId("labelPane");

		// Playerstatus Labels
		health = new Label("Health");
		health.setId("statLabel");
		healthValue = new Label("" + gameService.getPlayer().getLifepoints());
		healthValue.setId("valueLabel");
		armor = new Label("Armor");
		armor.setId("statLabel");
		armorValue = new Label("" + gameService.getPlayer().getArmor());
		armorValue.setId("valueLabel");
		evade = new Label("Evade");
		evade.setId("statLabel");
		evadeValue = new Label("" + gameService.getPlayer().getEvade() + "%");
		evadeValue.setId("valueLabel");
		strength = new Label("Strength");
		strength.setId("statLabel");
		strengthValue = new Label("" + gameService.getPlayer().getStrength());
		strengthValue.setId("valueLabel");
		agility = new Label("Agility");
		agility.setId("statLabel");
		agilityValue = new Label("" + gameService.getPlayer().getAgility());
		agilityValue.setId("valueLabel");
		knowledge = new Label("Knowledge");
		knowledge.setId("statLabel");
		knowledgeValue = new Label("" + gameService.getPlayer().getKnowledge());
		knowledgeValue.setId("valueLabel");

		// Add Playerstatus Labels
		statusPane.add(health, 0, 0);
		statusPane.add(healthValue, 1, 0);
		statusPane.add(armor, 0, 1);
		statusPane.add(armorValue, 1, 1);
		statusPane.add(evade, 0, 2);
		statusPane.add(evadeValue, 1, 2);
		statusPane.add(strength, 0, 3);
		statusPane.add(strengthValue, 1, 3);
		statusPane.add(agility, 0, 4);
		statusPane.add(agilityValue, 1, 4);
		statusPane.add(knowledge, 0, 5);
		statusPane.add(knowledgeValue, 1, 5);

		rightSplitPane.getItems().addAll(mapPane, statusPane);

		// *************************************************MainBox / MainSplitBox ADD**************************************
		mainBox.getChildren().addAll(menuBar, mainSplitPane);
		mainSplitPane.getItems().addAll(leftSplitPane, rightSplitPane);

		mainScene = new Scene(mainBox, 900, 700);
		mainScene.getStylesheets().add("https://fonts.googleapis.com/css2?family=Press+Start+2P&display=swap");
		mainScene.getStylesheets().add("file:ressourcen/style.css");
		startScene.getStylesheets().add("https://fonts.googleapis.com/css2?family=Press+Start+2P&display=swap");
		startScene.getStylesheets().add("file:ressourcen/style.css");
		window.setScene(startScene);
		window.show();

		// **************************************Bewegungssteuerung***************************************
		mainBox.setOnKeyPressed(e -> {
			buttonAction(e);
		});

		// **************************************weitere Initialisierungen***********************************

		// SpielerObjekt initialisieren
		healthValue.setText("" + gameService.getPlayer().getLifepoints());
		gameService.initializePlayer();
		reloadMap();
	}

	/**
	 * L�scht alle Elemente in der Map und baut sie neu auf
	 */
	private void reloadMap() {
		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 10; j++) {
				mapPane.getChildren().removeAll();
				Image image = RoomService.GRID[i][j].getStatus();
				mapPane.add(new ImageView(image), i, j);
			}
		}
	}

	/**
	 * L�scht das Bild des Characters / Gegners und ersetzt es durch das �bergebene
	 * @param image
	 */
	private void reloadImage(Image image) {
		imagePane.getChildren().remove(0);
		imagePane.getChildren().add(new ImageView(image));
	}

	/**
	 * 
	 * @param event
	 */
	private void buttonAction(KeyEvent event) {

		int move = 0;
//		System.out.println(RoomService.GRID[gameService.getPlayer().getCoordX()][gameService.getPlayer().getCoordY()]);
		
		if (canMove) {
			switch (event.getCode()) {
			case W:
				enemyService.moveEnemys();
				move = gameService.movePlayerUp();
				moveEvents(move);
				break;
			case A:
				enemyService.moveEnemys();
				move = gameService.movePlayerLeft();
				moveEvents(move);
				break;
			case S:
				enemyService.moveEnemys();
				move = gameService.movePlayerDown();
				moveEvents(move);
				break;
			case D:
				enemyService.moveEnemys();
				move = gameService.movePlayerRight();
				moveEvents(move);
				break;
			default:
				break;
			}
		}
	}

	/**
	 * 
	 * @param move
	 */
	private void moveEvents(int move) {
		statusText.setText("and again you walk through the corridors of Elysium...");
		
		if (move == 1) {
			eventStart(RoomService.GRID[gameService.getPlayer().getCoordX()][gameService.getPlayer().getCoordY()]
					.getEvent());
			canMove = false;
		}
		if (move == 2) {
			statusText.setText("");
			enemyName.setText(RoomService.GRID[gameService.getPlayer().getCoordX()][gameService.getPlayer().getCoordY()]
					.getEnemy().getName());
			combatStart(gameService.getPlayer(),
					RoomService.GRID[gameService.getPlayer().getCoordX()][gameService.getPlayer().getCoordY()]
							.getEnemy());
			canMove = false;
		}
		if (move == 3) {
			movePane.setVisible(false);
			statusText.setText("Wohoo\nYou have won the Game!");
			canMove = false;
		}
		reloadMap();
	}

	/**
	 * Startet und verwaltet ein Event Texte werden aus dem jeweiligen Event Objekt
	 * ausgelesen und gesetzt Buttons werden je nachdem wie sie ben�tigt werden auf
	 * sichtbar / unsichtbar gesetzt
	 * 
	 * @param event
	 * @param ja
	 * @param nein
	 * @param ok
	 * @param text
	 * @param move
	 */
	private void eventStart(Event event) {

		statusText.setText("" + event.getTextBegin());
		yes.setVisible(true);
		no.setVisible(true);
		movePane.setVisible(false);

		yes.setOnAction(e -> {
			statusText.setText(event.getTextEndA());
			yes.setVisible(false);
			no.setVisible(false);
			ok.setVisible(true);
		});
		
		no.setOnAction(e -> {
			statusText.setText(event.getTextEndB());
			yes.setVisible(false);
			no.setVisible(false);
			ok.setVisible(true);
		});

		ok.setOnAction(e -> {
			ok.setVisible(false);
			movePane.setVisible(true);
			statusText.setText("");
			RoomService.GRID[gameService.getPlayer().getCoordX()][gameService.getPlayer().getCoordY()].setHasEvent(false);
			canMove = true;
		});
	}

	/**
	 * Nimmt ein Spieler und ein Gegner Objekt entgegen
	 * 
	 * Der Kampf wird initialisiert und die ben�tigten Buttons eingeblendet
	 * 
	 * Je nach Aktion (Aktivierung eines Buttons) werden Methoden zum Kampfablauf aufgerufen
	 * 	und Texte angezeigt
	 * 
	 * Wenn der Kampf vorbei ist, werden die Angriffsbuttons wieder ausgeblendet
	 *   
	 * @param player
	 * @param enemy
	 * @param text
	 * @param move
	 * @param attackMelee
	 * @param attackRange
	 * @param block
	 * @param item
	 * @param flee
	 * @param startCombat
	 */
	private void combatStart(Player player, Enemy enemy) {
		
		reloadImage(enemy.getPortrait());
		statusText.setText(enemy.battleShout());

		ok.setVisible(true);
		movePane.setVisible(false);

		ok.setOnAction(e -> {
			ok.setVisible(false);
			attackPane.setVisible(true);
		});
		//***************************** Attack Melee***************************
		attackMelee.setOnAction(e -> {
			combatRound++;
			textInput += "Round: " + combatRound;
			textInput += "\n****************\n\n";
			textInput += player.attackMelee(enemy) + "\n";
			textInput += "\n****************\n\n";
			textInput += enemy.attackMelee(player) + "\n";
			textInput += "\n****************\n";

			healthValue.setText("" + gameService.getPlayer().getLifepoints());
			
			if (!player.isAlive()) {
				textInput = "You are dead :(";
				attackPane.setVisible(false);
			}

			if (!(enemy.isAlive())) {
				textInput = "\t*\n\nCongratulations!\n\nyou have won against the enemy\n\nyour reward is a wet handshake from yourself\n\n\t*";
				combatEnd = true;
			}

			if (combatEnd) {
				movePane.setVisible(true);
				attackPane.setVisible(false);
				combatEnd = false;
				reloadImage(new Image("file:ressourcen/Images/Player/Player.png"));
				RoomService.GRID[enemy.getCoordX()][enemy.getCoordY()].setHasEnemy(false);
				RoomService.GRID[enemy.getCoordX()][enemy.getCoordY()].setEnemy(null);
				canMove = true;
				enemyName.setText("Elysium...");
			}
			statusText.setText(textInput);
			textInput = "";
		});
		//***************************** Attack Range***************************
		attackRange.setOnAction(e -> {
			combatRound++;
			textInput += "Round: " + combatRound;
			textInput += "\n****************\n\n";
			textInput += player.attackRange(enemy) + "\n";
			textInput += "\n****************\n\n";
			textInput += enemy.attackMelee(player) + "\n";
			textInput += "\n****************\n";

			healthValue.setText("" + gameService.getPlayer().getLifepoints());
			
			if (!player.isAlive()) {
				textInput = "You are dead :(";
				attackPane.setVisible(false);
			}

			if (!(enemy.isAlive())) {
				textInput = "\t*\n\nCongratulations!\n\nyou have won against the enemy\n\nyour reward is a wet handshake from yourself\n\n\t*";
				combatEnd = true;
			}

			if (combatEnd) {
				movePane.setVisible(true);
				attackPane.setVisible(false);
				combatEnd = false;
				reloadImage(new Image("file:ressourcen/Images/Player/Player.png"));
				RoomService.GRID[enemy.getCoordX()][enemy.getCoordY()].setHasEnemy(false);
				RoomService.GRID[enemy.getCoordX()][enemy.getCoordY()].setEnemy(null);
				canMove = true;
				enemyName.setText("Elysium...");
			}
			statusText.setText(textInput);
			textInput = "";
		});
	
		//********************************* Flee*****************************
		flee.setOnAction(e -> {
			if(player.fleeAtempt()) {
				movePane.setVisible(true);
				attackPane.setVisible(false);
				statusText.setText("Your flee atempt was successfull!");
				reloadImage(new Image("file:ressourcen/Images/Player/Player.png"));
				canMove = true;
				enemyName.setText("Elysium...");
			} else {
				textInput = "\nYour flee attempt has failed!\n\nthe enemy attacks!\n\n";
				textInput += "\n****************\n\n";
				textInput += enemy.attackMelee(player) + "\n";
				textInput += "\n****************\n";
				statusText.setText(textInput);
				healthValue.setText("" + gameService.getPlayer().getLifepoints());
				
				if (!player.isAlive()) {
					statusText.setText("You are dead :(");
					attackPane.setVisible(false);
				}
			}
		});
	}

	private void closeProgram() {
		if (newExitBox.displayBox()) {
			window.close();
		}
	}
}
