package de.domga.elysium.frontend;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * Kleine PopUp Box die erschein wenn man das Spiel beenden m�chte
 * Fragt eine best�tigung des Benutzer ab
 * @author Dome
 *
 */
public class ExitBox {
	/** statischer Boolean der zur�ck zum Hauptfenster gegeben wird*/
	static boolean ret;
	
	/**
	 * Erstellt eine Box die den Benutzer abfragt ob er da Spiel wirklich beenden m�chte
	 * @return
	 */
	public boolean displayBox() {
			
			Stage window = new Stage();
			
			window.initModality(Modality.APPLICATION_MODAL);
			window.setTitle("");
			window.setMinWidth(250);
			
			Label label = new Label();
			label.setText("Confirm Exit?");
			label.setId("exitLabel");
			
			Button yesButton = new Button("Yes");
			Button noButton = new Button("No");
			
			yesButton.setOnAction(e -> {
				ret = true;
				window.close();
			});
			noButton.setOnAction(e -> {
				ret = false;
				window.close();
			});
			
			VBox layout = new VBox(10);
			layout.getChildren().addAll(label, yesButton, noButton);
			layout.setAlignment(Pos.CENTER);
			layout.setId("exitBoxLayout");
			
			Scene scene = new Scene(layout);
			scene.getStylesheets().add("https://fonts.googleapis.com/css2?family=Press+Start+2P&display=swap");
			scene.getStylesheets().add("file:ressourcen/style.css");
			window.setScene(scene);
			window.showAndWait();
			
			return ret;
		}
}
