package de.domga.elysium.backend.events;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Lie�t alle Events aus einer Textdatei und stellt sie zur Verf�gung
 *  
 * Stellt alternativ generierte Events zur verf�gung falls die Datei nicht existiert
 * 
 * @author Dome
 *
 */
public class EventDAOImplMitDatei implements EventDAO{

	/** Liste aller Events im Spiel*/
	private List<Event> events;
	
	/**
	 * Initialisiert die Arraylist der Events
	 */
	public EventDAOImplMitDatei() {
		events = new ArrayList<Event>();
	}
	
	/**
	 * Lie�t die ersten 3 Abschnitte aus einer Textdatei aus und schreibt sie in ein Event
	 * Das Event wird der Liste aller Events hinzugef�gt
	 * Danach werden die n�chsten 3 Abschnitte der Datei gelesen usw....
	 * Wird die Datei nicht gefunde, werden die automatisch generierten Events geladen
	 */
	@Override
	public List<Event> getAllEvents() {
		
		File eventFile =  new File("ressourcen/Events/events.txt");
		
		try(BufferedReader reader = new BufferedReader(new FileReader(eventFile))) {
			
			String line = reader.readLine();			
			int count = 0;
			
			while(true) {
				if (line == null) {
					break;
				}
				events.add(new Event());
				String temp = "";
								
				while(true) {
					if ((line == null) || (line.equalsIgnoreCase("NEWLINE"))) {
						line = reader.readLine();
						break;
					}
					
					temp += line;
					line = reader.readLine();
				}
				events.get(count).setTextBegin(temp);
				temp = "";
				
				while(true) {
					if ((line == null) || (line.equalsIgnoreCase("NEWLINE"))) {
						line = reader.readLine();
						break;
					}
					temp += line;
					line = reader.readLine();
				}
				events.get(count).setTextEndA(temp);
				temp = "";
				
				while(true) {
					if ((line == null) || (line.equalsIgnoreCase("NEWLINE"))) {
						line = reader.readLine();
						break;
					}
					temp += line;
					line = reader.readLine();
				}
				events.get(count).setTextEndB(temp);
				temp = "";
//				System.out.println(events.get(count));
				count++;
			}
		} catch(IOException ioe) {
			ioe.printStackTrace();
			EventMaker eventCreator = new EventMaker();
			events = eventCreator.getAllEvents();
		}
		
		return events;
	}
	

}
