package de.domga.elysium.backend.events;

import java.util.ArrayList;
import java.util.List;

/**
 * Generiert PLatzhalter Events als Alternative, wenn die Event Datei nicht gefunden wird
 * 
 * @author Dome
 *
 */
public class EventMaker {

	/** Liste aller Events*/
	private List<Event> allEvents = new ArrayList<Event>();
	
	/**
	 * Erstellt 10 Platzhalter Events
	 */
	public EventMaker() {
		for (int i = 1; i < 11; i++) {
			allEvents.add(new Event("You discovered event number " + i + " !", "you have chosen A ", "you have chosen B"));
		}
	}
	
	public List<Event> getAllEvents() {
		return allEvents;
	}
}
