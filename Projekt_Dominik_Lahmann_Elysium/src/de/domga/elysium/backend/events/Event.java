package de.domga.elysium.backend.events;

/**
 * VALUE Klasse Event
 * Beinhaltet alle ben�tigten Texte und Werte die f�r Events 
 * 	ben�tigt werden
 * 
 * In Planung: Statuswerte des Spielers �ndern, Gegner spawnen etc...
 * @author Dome
 *
 */
public class Event {
	/** Er�ffnungstext des Events*/
	private String textBegin;
	/** Text f�r Entscheidung A*/
	private String textEndA;
	/** Text f�r Entscheidung B*/
	private String textEndB;
	/** X Koordinate*/
	private int coordX;
	/** Y Koordinate*/
	private int coordY;
	
	/** 
	 * @param textBegin
	 * @param textEndA
	 * @param textEndB
	 */
	public Event (String textBegin, String textEndA, String textEndB) {
		this.textBegin = textBegin;
		this.textEndA = textEndA;
		this.textEndB = textEndB;
		this.coordX = 0;
		this.coordY = 0;
	}
	/** Default Konstruktor*/
	public Event () {
		this.textBegin = "";
		this.textEndA = "";
		this.textEndB = "";
		this.coordX = 0;
		this.coordY = 0;
	}
	
	/**
	 * @return the text
	 */
	public String getTextBegin() {
		return textBegin;
	}

	/**
	 * @param text the text to set
	 */
	public void setTextBegin(String text) {
		this.textBegin = text;
	}
	
	/**
	 * @return the text
	 */
	public String getTextEndA() {
		return textEndA;
	}

	/**
	 * @param text the text to set
	 */
	public void setTextEndA(String text) {
		this.textEndA = text;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getTextEndB() {
		return textEndB;
	}

	/**
	 * @param text the text to set
	 */
	public void setTextEndB(String text) {
		this.textEndB = text;
	}

	/**
	 * @return the coordX
	 */
	public int getCoordX() {
		return coordX;
	}

	/**
	 * @param coordX the coordX to set
	 */
	public void setCoordX(int coordX) {
		this.coordX = coordX;
	}

	/**
	 * @return the coordY
	 */
	public int getCoordY() {
		return coordY;
	}

	/**
	 * @param coordY the coordY to set
	 */
	public void setCoordY(int coordY) {
		this.coordY = coordY;
	}

	@Override
	public String toString() {
		return "Event [textBegin=" + textBegin + ", textEndA=" + textEndA + ", textEndB=" + textEndB + ", coordX=" + coordX
				+ ", coordY=" + coordY + "]";
	}
}
