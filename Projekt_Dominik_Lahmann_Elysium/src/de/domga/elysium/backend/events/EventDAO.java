package de.domga.elysium.backend.events;

import java.util.List;

/**
 * DAO Interface f�r den Zugriff auf die Events
 * @author Dome
 *
 */
public interface EventDAO {
	/** Liste aller Events des Spiels*/
	List<Event> getAllEvents();
}
