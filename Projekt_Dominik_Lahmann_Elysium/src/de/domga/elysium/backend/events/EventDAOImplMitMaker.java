package de.domga.elysium.backend.events;

import java.util.List;


/**
 * Implementiert das DAO Interface f�r Events und stellt die Daten f�r die Anwendung zur Verf�gung
 * 
 * Eine alte Klasse die nicht mehr ben�tigt wird. Siehe -> EventDAOImplMitDatei.java
 * 
 * Wird zu dokumentationszwecken noch behalten
 * 
 * @author Dome
 *
 */
public class EventDAOImplMitMaker implements EventDAO{

	private EventMaker eventCreator;
	private List<Event> events;
	
	public EventDAOImplMitMaker() {
		eventCreator = new EventMaker();
		events = eventCreator.getAllEvents();
	}
	
	@Override
	public List<Event> getAllEvents() {
		return events;
	}

}
