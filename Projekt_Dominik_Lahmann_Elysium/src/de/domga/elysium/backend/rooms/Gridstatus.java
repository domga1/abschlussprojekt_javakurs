package de.domga.elysium.backend.rooms;

import javafx.scene.image.Image;

/**
 * Stellt die Pfade der Bilder f�r das GRID zur Verf�gung
 * @author Dome
 *
 */
public enum Gridstatus {

	FOG ("file:ressourcen/Images/Grid/FOG.png"),
	PLAYER ("file:ressourcen/Images/Grid/PLAYER.png"),
	ENEMY ("file:ressourcen/Images/Grid/ENEMY.png"),
	VISITED ("file:ressourcen/Images/Grid/VISITED.png"),
	BLOCKED ("file:ressourcen/Images/Grid/BLOCKED.png"),
	EVENT ("file:ressourcen/Images/Grid/EVENT.png"),
	ENDING ("file:ressourcen/Images/Grid/ENDING.png");
	
	/** Bildpfad*/
	private String path;
	/** Bild f�r das GRID*/
	private Image image; 
    
	private Gridstatus (String path) {
		this.path = path;
		this.image = new Image (path);
	}
	
	/***
	 * 
	 * @return Image
	 */
	public Image getImage() {
		return image;
	}
}
