package de.domga.elysium.backend.rooms;

import java.util.List;

/**
 * Implementiert das DAO Interface f�r R�ume und stellt die Daten f�r die Anwendung zur Verf�gung
 * @author Dome
 *
 */
public class RoomDAOImplMitMaker implements RoomDAO{

	private RoomMaker roomCreator;
	private List<Room> rooms;
	
	public RoomDAOImplMitMaker() {
		roomCreator = new RoomMaker();
		rooms = roomCreator.getAllRooms();
	}
	
	@Override
	public List<Room> getAllRooms() {
		return rooms;
	}
}
