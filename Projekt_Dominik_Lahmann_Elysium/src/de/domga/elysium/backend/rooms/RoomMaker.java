package de.domga.elysium.backend.rooms;

import java.util.ArrayList;
import java.util.List;

/**
 * Erstellt R�ume f�r das Grid
 * Default:
		roomID = 0;
		coordX = 0;
		coordY = 0;
		enemy = false;
		player = false;
		north = true;
		east = true;
		south = true;
		west = true;
 * @author Dome
 *
 */
public class RoomMaker {
	/** Liste aller R�ume*/
	private List<Room> allRooms = new ArrayList<Room>();
	/** Raum ID*/
	private int id = 1;
	
	public RoomMaker() {
		for (int i = 1; i < 11; i++) {
			for (int j = 1; j < 11; j++) {
				allRooms.add(new Room(id,i,j));
				id++;
			}
		}
	}
	
	/**
	 * 
	 * @return allRooms
	 */
	public List<Room> getAllRooms() {
		return allRooms;
	}
}
