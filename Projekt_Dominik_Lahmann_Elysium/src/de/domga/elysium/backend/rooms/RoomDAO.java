package de.domga.elysium.backend.rooms;

import java.util.List;

/**
 * DAO Interface f�r den Zugriff auf die R�ume
 * @author Dome
 *
 */
public interface RoomDAO {

	List<Room> getAllRooms();

}
