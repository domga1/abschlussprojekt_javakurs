package de.domga.elysium.backend.rooms;

import de.domga.elysium.backend.character.enemy.Enemy;
import de.domga.elysium.backend.events.Event;
import javafx.scene.image.Image;

/**
 * DAO
 * VALUE Klasse zum Erstellen von einzelnen R�umen
 * 
 * Geplant: Die Booleans der Himmelsrichtungen benutzen um T�ren zu simmulieren
 * 
 * @author Dome
 *
 */
public class Room {

	/** Einzigartige RaumID*/
	private int roomID;
	
	/** X Koordinate des Raumes*/
	private int coordX;
	/** Y Koordinate des Raumes*/
	private int coordY;
	
	/** Boolean zum �berpr�fen ob ein Gegner im Raum ist*/
	private boolean hasEnemy;
	/** Boolean zum �berpr�fen ob der Spieler im Raum ist*/
	private boolean player;
	/** Boolean zum �berpr�fen ob dem Raum ein Event zugewiesen worden ist*/
	private boolean hasEvent;
	/** Boolean zum �berpr�fen ob der Raum der "Endraum" ist*/
	private boolean hasEnding;
	/** Boolean zum �berpr�fen ob der Raum betretbar / geblockt ist*/
	private boolean blocked;
	/** Beinhaltet das aktuelle Image f�r das UI Feld*/
	private Image status;
	/** Beinhaltet das Event f�r den Raum*/
	private Event event;
	/** Beinhaltet den Gegner dieses Raumes*/
	private Enemy enemy;
	
	public Room () {
		roomID = 0;
		coordX = 0;
		coordY = 0;
		hasEnemy = false;
		player = false;
		hasEvent = false;
		this.blocked = false;
		status = null;
		event = null;
		enemy = null;
	}

	/**
	 * @param roomID
	 * @param coordX
	 * @param coordY
	 */
	public Room(int roomID, int coordX, int coordY) {
		this.roomID = roomID;
		this.coordX = coordX;
		this.coordY = coordY;
		this.hasEnemy = false;
		this.player = false;
		this.hasEvent = false;
		this.blocked = false;
		this.status = null;
		this.event = null;
		this.enemy = null;
	}

	/**
	 * @return the roomID
	 */
	public int getRoomID() {
		return roomID;
	}

	/**
	 * @param roomID the roomID to set
	 */
	public void setRoomID(int roomID) {
		this.roomID = roomID;
	}

	/**
	 * @return the coordX
	 */
	public int getCoordX() {
		return coordX;
	}

	/**
	 * @param coordX the coordX to set
	 */
	public void setCoordX(int coordX) {
		this.coordX = coordX;
	}

	/**
	 * @return the coordY
	 */
	public int getCoordY() {
		return coordY;
	}

	/**
	 * @param coordY the coordY to set
	 */
	public void setCoordY(int coordY) {
		this.coordY = coordY;
	}

	/**
	 * @return the hasEnemy
	 */
	public boolean isHasEnemy() {
		return hasEnemy;
	}

	/**
	 * @param hasEnemy the hasEnemy to set
	 */
	public void setHasEnemy(boolean hasEnemy) {
		this.hasEnemy = hasEnemy;
	}

	/**
	 * @return the player
	 */
	public boolean isPlayer() {
		return player;
	}

	/**
	 * @param player the player to set
	 */
	public void setPlayer(boolean player) {
		this.player = player;
	}

	/**
	 * @return the hasevent
	 */
	public boolean isHasEvent() {
		return hasEvent;
	}

	/**
	 * @param hasevent the hasevent to set
	 */
	public void setHasEvent(boolean hasevent) {
		this.hasEvent = hasevent;
	}

	/**
	 * @return the status
	 */
	public Image getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(Image status) {
		this.status = status;
	}

	/**
	 * @return the event, can be null if no event is in the room!
	 */
	public Event getEvent() {
		return event;
	}

	/**
	 * @param event the event to set
	 */
	public void setEvent(Event event) {
		this.event = event;
	}

	/**
	 * @return the enemy, can be null if no enemy is in the room!
	 */
	public Enemy getEnemy() {
		return enemy;
	}

	/**
	 * @param enemy the enemy to set
	 */
	public void setEnemy(Enemy enemy) {
		this.enemy = enemy;
	}

	/**
	 * @return the hasEnding
	 */
	public boolean isHasEnding() {
		return hasEnding;
	}

	/**
	 * @param hasEnding the hasEnding to set
	 */
	public void setHasEnding(boolean hasEnding) {
		this.hasEnding = hasEnding;
	}

	/**
	 * @return the blocked
	 */
	public boolean isBlocked() {
		return blocked;
	}

	/**
	 * @param blocked the blocked to set
	 */
	public void setBlocked(boolean blocked) {
		this.blocked = blocked;
	}

	@Override
	public String toString() {
		return "Room [roomID=" + roomID + ", coordX=" + coordX + ", coordY=" + coordY + ", hasEnemy=" + hasEnemy
				+ ", player=" + player + ", hasEvent=" + hasEvent + ", hasEnding=" + hasEnding + ", blocked=" + blocked + "]";
	}

}
