package de.domga.elysium.backend.character.enemy;

import javafx.scene.image.Image;
/**
 * Beinhaltet die Attribute sowie den Battleshout des Roombo Gegners
 * @author Dome
 *
 */
public class Roombo extends Enemy{

	public Roombo () {
		super("Roombo", 1);
		this.damage = 10;
		this.armor = 3;
		this.lifepointsmax = 60;
		this.lifepoints = 60;
		this.portrait = new Image("file:ressourcen/Images/Enemy/Roombo.png");
	}
	
	/**
	 * Überschriebene Battleshout Methode der Elternklasse
	 * Gibt den Battleshout des Roombo zurück
	 */
	@Override
	public String battleShout() {
		return "A wild Roombo appears!";
	}
}
