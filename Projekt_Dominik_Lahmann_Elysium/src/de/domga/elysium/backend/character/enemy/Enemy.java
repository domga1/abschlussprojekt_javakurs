package de.domga.elysium.backend.character.enemy;

import java.util.Random;

import de.domga.elysium.backend.character.Fighter;
import de.domga.elysium.backend.rooms.Gridstatus;
import de.domga.elysium.middletier.RoomService;
import javafx.scene.image.Image;

/**
 * Elternklasse f�r Gegner
 * @author Dome
 *
 */
public class Enemy extends Fighter{
	
	/** Bild des Gegners*/
	protected Image portrait;
	/** Bewegungsgeschwindigkeit des Gegners auf dem GRID*/
	private int moveSpeed;
	
	/**
	 * 
	 * @param name
	 * @param moveSpeed
	 */
	public Enemy(String name, int moveSpeed) {
		super(name);
		this.moveSpeed = moveSpeed;
		this.portrait = null;
	}
	
	/**
	 * 	Die Bewegungsrichtung und Geschwindigkeit des Gegners wird festgelegt
	 * 		und mit diesen Informationen "move" aufgerufen
	 */
	public void moveEnemyRandom() {
		
		Random rand = new Random();
		
		int auswahl = rand.nextInt(4);
		switch(auswahl) {
			case 0:
				move(this.getCoordX()+moveSpeed, this.getCoordY());
				break;
			case 1:
				move(this.getCoordX()-moveSpeed, this.getCoordY());
				break;
			case 2:
				move(this.getCoordX(), this.getCoordY()-moveSpeed);
				break;
			case 3:
				move(this.getCoordX(), this.getCoordY()+moveSpeed);
				break;
		}
		
	}
	
	/**
	 * Bewegt den Gegner auf dem GRID
	 * @param x
	 * @param y
	 */
	private void move (int x, int y) {
		if (inBorder(x,y)) {
			if (!((RoomService.GRID[x][y].isHasEvent())) && (!(RoomService.GRID[x][y].isHasEnemy())) &&
					(!(RoomService.GRID[x][y].isBlocked())) && (!(RoomService.GRID[x][y].isHasEnding()))){
				RoomService.GRID[this.getCoordX()][this.getCoordY()].setHasEnemy(false);
				RoomService.GRID[this.getCoordX()][this.getCoordY()].setEnemy(null);
				RoomService.GRID[this.getCoordX()][this.getCoordY()].setStatus(Gridstatus.FOG.getImage());
	
				RoomService.GRID[x][y].setHasEnemy(true);
				RoomService.GRID[x][y].setEnemy(this);
				RoomService.GRID[x][y].setStatus(Gridstatus.ENEMY.getImage());
				this.setCoordX(x);
				this.setCoordY(y);
				} 
			}
	}
	
	/**
	 * �berpr�ft ob die entgegengenommenen Koordinaten im GRID liegen
	 * 	und gibt einen Boolean zur�ck
	 * @param x
	 * @param y
	 * @return
	 */
	private boolean inBorder(int x, int y) {		
		if (x <= 9 && x >= 0 && y <= 9 && y >= 0) {
			return true;
		}
		return false;
	}

	/**
	 * �berschriebene Nahkampfangriffs Methode der Elternklasse
	 * berechnet den Schaden den der Fighter einem anderen zuf�gt
	 * 	und ruft die Methode "takeDamage" auf
	 * Gibt einen String zur�ck
	 */
	@Override
	public String attackMelee (Fighter player) {
		String damagetaken = player.takeDamage(damage);
		return "" + name + " attacks for " + (damage - player.getArmor() + " dmg") + "\n\n" 
				+ damagetaken
				+ "\n\nYou have " + player.getLifepoints() + " Lifepoints left\n";
	}
	
	/**
	 * Verrechnet den Schaden den der Fighter bekommen hat und 
	 * 	�berpr�ft ob der Fighter noch am leben ist
	 * 
	 * gibt einen String zur�ck
	 * @param da
	 * @return
	 */
	@Override
	public String takeDamage(double da) {
		double damage = da - armor;
		lifepoints -= damage;
		if (lifepoints <= 0) {
			alive = false;
		}
		return "" + name + " takes " + da + " (-" + armor + ") -> " + damage + " dmg";
	}

	/**
	 * Gibt den Default Text f�r den Battleshout zur�ck
	 * @return the combatTextStart
	 */
	public String battleShout() {
		return " Du stehst einem " + name + " gegen�ber";
	}

	/**
	 * @return the portrait
	 */
	public Image getPortrait() {
		return portrait;
	}

	/**
	 * @param portrait the portrait to set
	 */
	public void setPortrait(Image portrait) {
		this.portrait = portrait;
	}
}
