package de.domga.elysium.backend.character.enemy;

import javafx.scene.image.Image;

/**
 * Beinhaltet die Attribute sowie den Battleshout des KillerBot Gegners
 * @author Dome
 *
 */
public class KillerBot extends Enemy{

	public KillerBot() {
		super("KillerBot Nr. 5", 2);
		this.damage = 30;
		this.armor = 10;
		this.lifepointsmax = 150;
		this.lifepoints = 150;
		this.portrait = new Image("file:ressourcen/Images/Enemy/KillerBot.png");
	}
	
	/**
	 * Überschriebene Battleshout Methode der Elternklasse
	 * Gibt den Battleshout des KillerBots zurück
	 */
	@Override
	public String battleShout() {
		return "Run!\nYou cannot win";
	}
}
