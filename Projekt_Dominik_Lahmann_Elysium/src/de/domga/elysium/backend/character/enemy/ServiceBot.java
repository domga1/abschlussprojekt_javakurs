package de.domga.elysium.backend.character.enemy;

import javafx.scene.image.Image;
/**
 * Beinhaltet die Attribute sowie den Battleshout des ServiveBot Gegners
 * @author Dome
 *
 */
public class ServiceBot extends Enemy{

	public ServiceBot() {
		super("Service BOT T100", 1);
		this.damage = 15;
		this.armor = 4;
		this.lifepointsmax = 100;
		this.lifepoints = 100;
		this.portrait = new Image("file:ressourcen/Images/Enemy/ServiceBot.png");
	}

	/**
	 * Überschriebene Battleshout Methode der Elternklasse
	 * Gibt den Battleshout des ServiceBots zurück
	 */
	@Override
	public String battleShout() {
		return "The Service Bot is out of control !\nHelp him to become electronic waste";
	}
}
