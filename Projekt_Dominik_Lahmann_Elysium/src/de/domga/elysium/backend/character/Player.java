package de.domga.elysium.backend.character;

import java.util.Random;

/**
 * Spielerklasse
 * 
 * Beinhaltet momentan nur die �berschriebenen Kampf Methoden
 * 	und Spielerwerte zu Schaden und R�stung
 * 
 * Geplant: Weitere Ausbau der Spielerklasse mit mehr Statuswerten und mehr
 * 	m�glichkeiten im Kampf
 * @author Dome
 *
 */
public class Player extends Fighter{
	/** St�rke des Spielers, bestimmt den Schaden*/
	private int strength;
	/** Beweglichkeit des Spielers, bestimmt die Ausweichchance*/
	private int agility;
	/** Wissen des Spielers, beeinflusst die Events*/
	private int knowledge;
	
	/**
	 * Default Konstruktor
	 */
	public Player() {
		super("Player");
		this.setCoordX(4);
		this.setCoordY(9);
		this.damage = 10;
		this.armor = 5;
		this.strength = 10;
		this.agility = 10;
		this.knowledge = 5;
		
		this.evade += agility;
	}
	
	/**
	 * �berschriebene Nahkampfangriffs Methode der Elternklasse
	 * berechnet den Schaden den der Spieler einem Gegner anderen zuf�gt
	 * 	und ruft die Methode "takeDamage" auf
	 * Der schaden wird durch einen Zufallsfaktor manipuliert
	 * Gibt einen String zur�ck
	 */
	@Override
	public String attackMelee (Fighter enemy) {
		Random rand = new Random();
		int manipulator = -5 + rand.nextInt(10); 
		String damagetaken = enemy.takeDamage(damage + strength + manipulator);
		return "You attack for " + ((damage + strength + manipulator) - enemy.getArmor()) + "\n\n" + damagetaken
				+ "\n\n" + enemy.getName() + " has " + enemy.getLifepoints() + " Lifepoints left\n";
	}
	
	/**
	 * berechnet den Fernkampf Schaden den der Spieler einem anderen Gegner zuf�gt
	 * 	und ruft die Methode "takeDamage" auf
	 * Der schaden wird durch einen Zufallsfaktor manipuliert
	 * Gibt einen String zur�ck
	 */
	public String attackRange (Fighter enemy) {
		Random rand = new Random();
		int manipulator = -15 + rand.nextInt(31); 
		String damagetaken = enemy.takeDamage(damage + strength + manipulator);
		return "You attack for " + ((damage + strength + manipulator) - enemy.getArmor()) + "\n\n" + damagetaken
				+ "\n\n" + enemy.getName() + " has " + enemy.getLifepoints() + " Lifepoints left\n";
	}
	
	/**
	 * �berpr�ft ob der Spieler ausweichen kann
	 * 
	 * Verrechnet den Schaden den der Spieler bekommen hat und 
	 * 	�berpr�ft ob er noch am leben ist
	 * 
	 * gibt einen String zur�ck
	 * @param da
	 * @return 
	 */
	@Override
	public String takeDamage(double da) {
		Random rand = new Random();
		if (rand.nextInt(100) > (this.agility + this.evade)) {
			double damage = da - armor;
			lifepoints -= damage;
			if (lifepoints <= 0) {
				alive = false;
			}
			return "You get " + da + " (-" + armor + ") -> " + damage + " dmg";
		}
		return "You evaded succesfully!!!";
	}
		
	/**
	 * Errechnen ob der Spieler es schafft zu fliehen
	 * Wird durch eine Zufallszahl berechnet
	 * Gibt einen boolean zur�ck
	 * @return
	 */
	public boolean fleeAtempt() {
		Random rand = new Random();
		if (rand.nextInt(100) < (evade*2)) {
			return true;
		}
		return false;
	}

	/**
	 * @return the strength
	 */
	public int getStrength() {
		return strength;
	}

	/**
	 * @param strength the strength to set
	 */
	public void setStrength(int strength) {
		this.strength = strength;
	}

	/**
	 * @return the agility
	 */
	public int getAgility() {
		return agility;
	}

	/**
	 * @param agility the agility to set
	 */
	public void setAgility(int agility) {
		this.agility = agility;
	}

	/**
	 * @return the knowledge
	 */
	public int getKnowledge() {
		return knowledge;
	}

	/**
	 * @param knowledge the knowledge to set
	 */
	public void setKnowledge(int knowledge) {
		this.knowledge = knowledge;
	}

	@Override
	public String toString() {
		return super.toString() + " strength=" + strength + ", agility=" + agility + ", knowledge=" + knowledge + "]";
	}
	
	
}


