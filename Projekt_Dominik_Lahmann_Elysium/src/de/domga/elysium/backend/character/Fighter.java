package de.domga.elysium.backend.character;

/**
 * Elternklasse f�r den Spieler und die Gegner
 * 
 * 
 * 
 * @author Dome
 *
 */
public class Fighter {
	/** Lebenspunkte*/
	protected int lifepoints;
	/** maximale Lebenspunkte*/
	protected int lifepointsmax;
	/** Schaden der verursacht wird*/
	protected double damage;
	/** R�stungswert, der schaden verhindert*/
	protected double armor;
	/** Ausweichwert des Fighters in Prozent*/
	protected int evade;
	/** Name des Fighters*/
	protected String name;
	/** Boolean zum �berpr�fen ob der Fighter noch lebt*/
	protected boolean alive;
	/** X Koordinate auf dem GRID*/
	protected int coordX;
	/** Y Koordinate auf dem GRID*/
	protected int coordY;
	/***/
	
	/**
	 * @param name
	 * @param coordX
	 * @param coordY
	 */
	public Fighter() {
		name = "Basic Enemy";
		coordX = 1;
		coordY = 1;
		lifepoints = 100;
		lifepointsmax = 100;
		damage = 5;
		armor = 1;
		evade = 5;
		alive = true;
	}
	
	/**
	 * 
	 * @param name
	 */
	public Fighter(String name) {
		this.name = name;
		this.coordX = 0;
		this.coordY = 0;
		lifepoints = 100;
		lifepointsmax = 100;
		damage = 5;
		armor = 1;
		evade = 5;
		alive = true;
	}
	
	/**
	 * Berechnet den Schaden den der Fighter einem anderen zuf�gt
	 * 	und ruft die Methode "takeDamage" auf
	 * Gibt einen String zur�ck
	 */
	public String attackMelee (Fighter enemy) {
		enemy.takeDamage(damage);
		return "" + name + " trifft " + enemy.name + " f�r " + (damage - enemy.getArmor());
	}
	
	/**
	 * Verrechnet den Schaden den der Fighter bekommen hat und gibt einen String zur�ck
	 * @param da
	 * @return
	 */
	public String takeDamage(double da) {
		lifepoints -= da - armor;
		return "Du erleidest" + (da - armor) + " schaden";
	}
	
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the lifepoints
	 */
	public int getLifepoints() {
		return lifepoints;
	}

	/**
	 * @param lifepoints the lifepoints to set
	 */
	public void setLifepoints(int lifepoints) {
		this.lifepoints = lifepoints;
	}

	/**
	 * @return the lifepointsmax
	 */
	public int getLifepointsmax() {
		return lifepointsmax;
	}

	/**
	 * @param lifepointsmax the lifepointsmax to set
	 */
	public void setLifepointsmax(int lifepointsmax) {
		this.lifepointsmax = lifepointsmax;
	}

	/**
	 * @return the coordX
	 */
	public int getCoordX() {
		return coordX;
	}

	/**
	 * @param coordX the coordX to set
	 */
	public void setCoordX(int coordX) {
		this.coordX = coordX;
	}

	/**
	 * @return the coordY
	 */
	public int getCoordY() {
		return coordY;
	}

	/**
	 * @param coordY the coordY to set
	 */
	public void setCoordY(int coordY) {
		this.coordY = coordY;
	}

	/**
	 * @return the damage
	 */
	public double getDamage() {
		return damage;
	}

	/**
	 * @param damage the damage to set
	 */
	public void setDamage(double damage) {
		this.damage = damage;
	}

	/**
	 * @return the armor
	 */
	public double getArmor() {
		return armor;
	}

	/**
	 * @param armor the armor to set
	 */
	public void setArmor(double armor) {
		this.armor = armor;
	}
	
	/**
	 * @return the alive
	 */
	public boolean isAlive() {
		return alive;
	}

	/**
	 * @param alive the alive to set
	 */
	public void setAlive(boolean alive) {
		this.alive = alive;
	}

	/**
	 * @return the evade
	 */
	public double getEvade() {
		return evade;
	}

	/**
	 * @param evade the evade to set
	 */
	public void setEvade(int evade) {
		this.evade = evade;
	}

	@Override
	public String toString() {
		return "Fighter [lifepoints=" + lifepoints + ", lifepointsmax=" + lifepointsmax + ", damage=" + damage
				+ ", armor=" + armor + ", evade=" + evade + ", name=" + name + ", alive=" + alive + ", coordX=" + coordX
				+ ", coordY=" + coordY + "]";
	}
}
