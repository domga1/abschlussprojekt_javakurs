package de.domga.elysium.middletier;

import de.domga.elysium.backend.character.Player;
import de.domga.elysium.backend.rooms.Gridstatus;

/**
 * Initialisert das Spielerobjekt
 * Bewegt den Spieler auf der Karte (GRID)
 * @author Dome
 *
 */
public class GameService {

	/** Spielerobjekt*/
	private Player playerObject;
	private RoomService roomService;

	/**
	 * 
	 * @param roomService
	 */
	public GameService(RoomService roomService) {
		this.roomService = roomService;
		playerObject = new Player();
	}
	
	/**
	 * 
	 * @return Player Object
	 */
	public Player getPlayer() {
		return playerObject;
	}
	
	/**
	 * Setzt den Boolean "Player" des Startraumes
	 * Aktualisiert den Status des Raumes
	 */
	public void initializePlayer() {
		RoomService.GRID[playerObject.getCoordX()][playerObject.getCoordY()].setPlayer(true);
		roomService.updateStatus(playerObject.getCoordX(), playerObject.getCoordY(), Gridstatus.PLAYER.getImage());
	}
	
	/**
	 * �berpr�ft ob die Spielerbewegung innerhalb des GRIDs liegt und sie keinen
	 * 	gesperrten Raum trifft
	 * Gibt die R�ckgabe von movePlayer zur�ck an das UI
	 */
	public int movePlayerUp () {
		if (playerObject.getCoordY() > 0 && (!(RoomService.GRID[playerObject.getCoordX()][playerObject.getCoordY()-1].isBlocked()))) {
			return movePlayer(0);
		}
		return 0;
	}
	
	/**
	 * �berpr�ft ob die Spielerbewegung innerhalb des GRIDs liegt und sie keinen
	 * 	gesperrten Raum trifft
	 * Gibt die R�ckgabe von movePlayer zur�ck an das UI
	 */
	public int movePlayerDown () {
		if (playerObject.getCoordY() < 9 && (!(RoomService.GRID[playerObject.getCoordX()][playerObject.getCoordY()+1].isBlocked()))) {
			return movePlayer(1);
		}
		return 0;
	}
	
	/**
	 * �berpr�ft ob die Spielerbewegung innerhalb des GRIDs liegt und sie keinen
	 * 	gesperrten Raum trifft
	 * Gibt die R�ckgabe von movePlayer zur�ck an das UI
	 */
	public int movePlayerRight () {
		if (playerObject.getCoordX() < 9 && (!(RoomService.GRID[playerObject.getCoordX()+1][playerObject.getCoordY()].isBlocked()))) {
			return movePlayer(3);
		}
		return 0;
	}
	
	/**
	 * �berpr�ft ob die Spielerbewegung innerhalb des GRIDs liegt und sie keinen
	 * 	gesperrten Raum trifft
	 * Gibt die R�ckgabe von movePlayer zur�ck an das UI
	 */
	public int movePlayerLeft () {
		if (playerObject.getCoordX() > 0 && (!(RoomService.GRID[playerObject.getCoordX()-1][playerObject.getCoordY()].isBlocked()))) {
			return movePlayer(2);
		}
		return 0;
	}
	
	/**
	 * Setzt den aktuellen Raum auf VISITED und den Playerstatus auf false
	 * aktualisiert die Spielerkoordinaten
	 * �berpr�ft ob in dem neuen Raum ein Event, ein Gegner oder das Ende ist
	 * 		und gibt den entsprechenden Integer zur�ck
	 * 
	 * move: 0->up 		1->down 	2->left 	3->right
	 * @return
	 */
	public int movePlayer(int move) {
		
		RoomService.GRID[playerObject.getCoordX()][playerObject.getCoordY()].setPlayer(false);
		roomService.updateStatus(playerObject.getCoordX(), playerObject.getCoordY(), Gridstatus.VISITED.getImage());
		
		switch (move) {
			case 0:
				playerObject.setCoordY(playerObject.getCoordY()-1);
				break;
			case 1:
				playerObject.setCoordY(playerObject.getCoordY()+1);
				break;
			case 2:
				playerObject.setCoordX(playerObject.getCoordX()-1);
				break;
			case 3:
				playerObject.setCoordX(playerObject.getCoordX()+1);
				break;
		}
		
		RoomService.GRID[playerObject.getCoordX()][playerObject.getCoordY()].setPlayer(true);
		roomService.updateStatus(playerObject.getCoordX(), playerObject.getCoordY(), Gridstatus.PLAYER.getImage());
		
		if (RoomService.GRID[playerObject.getCoordX()][playerObject.getCoordY()].isHasEvent()) {
			return 1;
		}
		if (RoomService.GRID[playerObject.getCoordX()][playerObject.getCoordY()].isHasEnemy()) {
			return 2;
		}
		if (RoomService.GRID[playerObject.getCoordX()][playerObject.getCoordY()].isHasEnding()) {
			return 3;
		}
		return 0;
	}
}
