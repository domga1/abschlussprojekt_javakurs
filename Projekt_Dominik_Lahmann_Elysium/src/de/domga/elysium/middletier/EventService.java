package de.domga.elysium.middletier;

import java.util.List;
import java.util.Random;

import de.domga.elysium.backend.events.Event;
import de.domga.elysium.backend.events.EventDAO;
import de.domga.elysium.backend.rooms.Gridstatus;

/**
 * F�gt die Events den spezifischen R�umen hinzu
 * @author Dome
 *
 */
public class EventService {
	/** Liste aller Events*/
	private List<Event> allEvents;
	/** Quelle f�r allEvents*/
	private EventDAO eventSource;
	
	/**
	 * Generiert zuf�llige Koordinaten innerhalb des GRIDs und
	 * 	f�gt das Event dem ausgew�hlten Raum hinzu
	 * @param eventSource
	 */
	public EventService (EventDAO eventSource) {
		this.eventSource = eventSource;
		allEvents = eventSource.getAllEvents();
		
		int count = 0;
		Random rand = new Random();
		
		while (true) {
			if (count == allEvents.size()) {
				break;
			}
			int x = rand.nextInt(9);
			int y = rand.nextInt(9);
			if ((!(RoomService.GRID[x][y].isHasEvent()) && (!(RoomService.GRID[x][y].isHasEnding()))) && (!(RoomService.GRID[x][y].isBlocked()))){
				RoomService.GRID[x][y].setHasEvent(true);
				RoomService.GRID[x][y].setEvent(allEvents.get(count));
				RoomService.GRID[x][y].setStatus(Gridstatus.EVENT.getImage());
				count++;
			}
		}		
	}
	
	/**
	 * 
	 * @return allEvents
	 */
	public List<Event> getAllEvents() {
		return allEvents;
	}
}
