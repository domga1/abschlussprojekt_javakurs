package de.domga.elysium.middletier;

import java.util.List;
import java.util.Random;

import de.domga.elysium.backend.events.Event;
import de.domga.elysium.backend.events.EventDAO;
import de.domga.elysium.backend.rooms.Gridstatus;
import de.domga.elysium.backend.rooms.Room;
import de.domga.elysium.backend.rooms.RoomDAO;
import javafx.scene.image.Image;

/**
 * Erstellt ein statisches Array aus R�umen -> GRID
 * Aktualisiert den Status (Images) der R�ume
 * @author Dome
 *
 */
public class RoomService {
	/** Liste aller R�ume*/
	private List<Room> allRooms;
	private RoomDAO roomSource;
	/** Hilfsvariable zum Itterieren durch die allRooms Liste*/
	private int index = 0;
	
	/** 
	 * Das GRID (die Karte) des Spiels
	 * Beinhaltet alle R�ume
	 */
	public static Room[][] GRID = new Room[10][10];
	
	/**
	 * Speichert alle R�ume der allRooms Liste in das statische Array
	 * 
	 * Setzt zuf�llig verteilt eine bestimmte Anzahl der R�ume auf den Status geblockt / nicht betretbar
	 * 
	 * F�gt das Ende des Spiel in einen zuf�lligen Raum am oberen Ende des GRIDs ein
	 * @param roomSource
	 */
	public RoomService(RoomDAO roomSource) {
		this.roomSource = roomSource;
		allRooms = roomSource.getAllRooms();
		
		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 10; j++) {
				GRID[i][j] = allRooms.get(index);
				GRID[i][j].setStatus(Gridstatus.FOG.getImage());
				index++;
			}
		}
		
		//Endraum einbauen
		Random rand = new Random();
		int randX = rand.nextInt(10);
		int randY = 1;
		GRID[randX][randY].setHasEnding(true);
		GRID[randX][randY].setStatus(Gridstatus.ENDING.getImage());
		
		//Geblockte R�ume einbauen
		int count = 1;		
		while (count < 15) {
			randX = rand.nextInt(10);
			randY = rand.nextInt(9);
			if ((!(GRID[randX][randY].isBlocked())) && (!(GRID[randX][randY].isHasEnding()))) {
				GRID[randX][randY].setStatus(Gridstatus.BLOCKED.getImage());
				GRID[randX][randY].setBlocked(true);
				count++;
			}
		}
	}
	
	/**
	 * 
	 * @return allRooms
	 */
	public List<Room> getAllRooms() {
		return allRooms;
	}
	
	/**
	 * Aktualisiert das Bild des Raumes auf dem GRID 
	 * 	mit dem �bergebenen Image
	 * @param x
	 * @param y
	 * @param i
	 */
	public void updateStatus (int x, int y, Image i) {
		GRID[x][y].setStatus(i);
	}
}
