package de.domga.elysium.middletier;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import de.domga.elysium.backend.character.enemy.Enemy;
import de.domga.elysium.backend.character.enemy.KillerBot;
import de.domga.elysium.backend.character.enemy.Roombo;
import de.domga.elysium.backend.character.enemy.ServiceBot;
import de.domga.elysium.backend.rooms.Gridstatus;

/**
 * Managed die Feinde im Spiel
 * F�gt die Feine den R�umen hinzu
 * @author Dome
 *
 */
public class EnemyService {

	/** Liste aller Gegnerobjekte im Spiel*/
	private List<Enemy> allEnemys = new ArrayList<Enemy>();
	
	/**
	 * Itteriert durch alle R�ume des GRIDs
	 * f�r jeden Raum wird mit einer wahrscheinlichkeit von 15%
	 * 	ein zuf�lliger Gegner generiert, sofern der Raum kein Event oder Ende enth�lt
	 * 	Das Gegnerobjekt und die Koordinaten werden der Methode "addEnemy" �bergeben
	 */
	public EnemyService() {
		
		Random rand = new Random();
		int count = 0;
		
		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 9; j++) {
				if ((!(RoomService.GRID[i][j].isHasEvent())) && (!(RoomService.GRID[i][j].isHasEnemy())) && 
						(!(RoomService.GRID[i][j].isHasEnding())) && (!(RoomService.GRID[i][j].isBlocked()))){
					if (rand.nextInt(100)+1 < 15) {
						int auswahl = rand.nextInt(5)+1;
						if (count == 10) {
							break;
						}
						count++;
						switch(auswahl)  {
							case 1:
								addEnemy(new Roombo(), i, j);
								break;
							case 2:
								addEnemy(new Roombo(), i, j);
								break;
							case 3:
								addEnemy(new ServiceBot(), i, j);
								break;
							case 4:
								addEnemy(new ServiceBot(), i, j);
								break;
							case 5:
								addEnemy(new KillerBot(), i, j);
								break;
						}
					}
				}
			}
		}
	}
	
	/**
	 * F�gt das �bergebene Gegnerobjekt dem Raum auf dem GRID hinzu
	 * F�gt das Gegnerobjekt in die allEnemys Liste hinzu
	 * @param enemy
	 * @param x
	 * @param y
	 */
	private void addEnemy(Enemy enemy, int x, int y) {
		enemy.setCoordX(x);
		enemy.setCoordY(y);
		RoomService.GRID[x][y].setHasEnemy(true);
		RoomService.GRID[x][y].setEnemy(enemy);
		RoomService.GRID[x][y].setStatus(Gridstatus.ENEMY.getImage());
		allEnemys.add(enemy);
	}
	
	/**
	 * F�r jeden Gegner auf dem GRID wird �berpr�ft, ob er noch am leben ist und
	 * 	ruft dann die "moveEnemyRandom" Methode auf
	 */
	public void moveEnemys () {
		allEnemys.forEach(e -> {
			if(e.isAlive()) {
				e.moveEnemyRandom();
			}
		});
	}
}
